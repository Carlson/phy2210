PHY2210 course materials
========================

This repository contains public course materials for PHY2210, General Physics 2, a calculus-based electricity and magnetism course following the [Matter and Interactions](http://matterandinteractions.org/) curriculum as taught by [Brant Carlson](https://www.carthage.edu/live/profiles/251-brant-carlson) at [Carthage College](https://www.carthage.edu/).

The files stored here are numbered by day of class and named with the general type of file.

- `*_notes.pdf` are lecture notes.

Warm-up questions, in-class questions, exams, and solutions will not be posted.  See the [course Google-site](https://sites.google.com/a/carthage.edu/physics-2210/) for other documents.

(If you are a faculty member interested in using these materials in your own work, let me know, as I'm happy to share the raw source material.)
